from flask_restful import Resource, Api,reqparse
from flask import Flask,jsonify, request
import logging as logger
import sqlite3

app = Flask(__name__)
api = Api(app)

# ვქმნით ბაზას სახელად DB.db
# ვქმნით ცხრილს სახელად countries
db = sqlite3.connect('DB.db')
db.execute('''CREATE TABLE IF NOT EXISTS countries (
                id INTEGER PRIMARY KEY,
                countryname TEXT NOT NULL,
                capital TEXT NOT NULL,
                population REAL NOT NULL,
                UNIQUE(countryname, capital, population)
            )''')
db.commit()
db.close()
print("table created")

# პარსერის საშვალებით ვპარსავთ გადმოცემულ მონაცემებს.
parser = reqparse.RequestParser()
parser.add_argument('countryname', type=str, help='Country Name Required.', required=True)
parser.add_argument('capital', type=str, help='City Name Required.', required=True)
parser.add_argument('population', type=float, help='Population Required.', required=True)

class to_do(Resource):
        # get მეთოდის საშვალებით მოგვაქვს ყველა მონაცემი და ასევე მხოლოდ ერთი მითითებული აიდის მიხედვით
    def get(self) -> jsonify:
        db = sqlite3.connect("DB.db")
        cursor = db.cursor()
        id = request.args.get('id')
        if id: 
            try:     
                data = cursor.execute(f"SELECT * FROM countries WHERE id = {id}")
                data = cursor.fetchone()
                cursor.close()
            except sqlite3.IntegrityError as msg:
                return jsonify(f"Error while getting row:  {msg}")
            
            if data:
                return jsonify(data)
            else:
                return jsonify("there is no data here")
        else:
            try:
                cursor.execute("SELECT * FROM countries")
                all_data = cursor.fetchall()
                cursor.close()
                return jsonify(all_data)
            except sqlite3.IntegrityError as msg:
                return jsonify(f"error while getting data from data base: {msg}")
            
        # post მეთოდის გამოყენების საშვალებით ვამატებთ მონაცემებს ბაზაში
    def post(self) -> jsonify:
        args = parser.parse_args()
        countryname = args['countryname']
        city = args['capital']
        population = args['population']
                
        if countryname and city and population:
            try:
                db = sqlite3.connect("DB.db")
                cursor = db.cursor()
                cursor.execute(f"INSERT INTO countries (countryname, capital, population) VALUES ('{countryname}', '{city}', {population})")
                db.commit()
                cursor.close()
                return jsonify("Succsessfuly add.")
            except sqlite3.IntegrityError as msg:
                return jsonify(f"exception: {msg}")                    
        else:
            return jsonify({'error': 'Title is required'}), 400
               
        # put მეთოდის გამოყენებით ვაკეთებთ მონაცემების ჩასწორებას ბაზაში.
    def put(self) -> jsonify:
        id = request.args.get('id')
        args = parser.parse_args()
        countryname = args['countryname']
        city = args['capital']
        population = args['population']
        if countryname and city and population:
            try:
                db = sqlite3.connect("DB.db")
                cursor = db.cursor()
                cursor.execute(f"UPDATE countries SET countryname ='{countryname}', capital ='{city}', population ={population} WHERE id = {id}")
                db.commit()
                cursor.close()
                return jsonify("Succsessfuly Edited: ", { 
                                                          "id": id, 
                                                          "countryname": countryname,
                                                          "capital": city,
                                                          "population": population
                                                        })
            except sqlite3.IntegrityError as msg:
                return jsonify(f"error while editing row: {msg}")
        
    # delete მეთოდის გამოყენებით ვშლით ბაზაში მონაცემებს აიდის მიხედვით.
    def delete(self) -> jsonify:
        try:
            id = request.args.get('id')
            db = sqlite3.connect("DB.db")
            cursor = db.cursor()
            cursor.execute(f"DELETE FROM countries WHERE id = {id}")
            db.commit()
            cursor.close()
            return jsonify(f"Succsesfully deleted row: {id}")
        except  sqlite3.IntegrityError as msg:
            return jsonify(f"exception: {msg}")                    
            

api.add_resource(to_do, '/todo')

if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)
